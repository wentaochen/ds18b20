#include "get_temp.h"

int get_temp(char *temp, int size)
{
	float		t;
	int		fd = -1;
	char		buf[128];
	char		*ptr = NULL;
	DIR		*dirp = NULL;
	struct dirent	*direntp = NULL;
	char 		path[64] = "/sys/bus/w1/devices/";
	char		chip_sn[32];
	int 		flag = 0;

	dirp = opendir(path);
	if(!dirp)
	{
		printf("Open folder %s failure: %s\n", path, strerror(errno));
		return -1;
	}

	while(NULL != (direntp = readdir(dirp)))
	{
		if(NULL != strstr(direntp->d_name, "28-"))
		{
			strncpy(chip_sn, direntp->d_name, sizeof(chip_sn));	
			flag = 1;
		}
	}

	closedir(dirp);

	if(!flag)
	{
		printf("Can not find SHT20 chipset.\n");
		return -2;
	}

	strncat(path, chip_sn, sizeof(path) - strlen(path));
	strncat(path, "/w1_slave", sizeof(path) - strlen(path));

	if((fd = open(path, O_RDONLY)) < 0)
	{
		printf("fd = %d\n", fd);
		printf("Open file failure: %s\n", strerror(errno));	
		return -3;
	}

	memset(buf, 0, sizeof(buf));
	if(read(fd, buf, sizeof(buf)) < 0)
	{
		printf("Read data from fd = %d, failure: %s\n", fd, strerror(errno));
	}

	ptr = strstr(buf, "t=");
	if(NULL == ptr)
	{
		printf("Can not find t= string\n");
	}

	ptr += 2;
	t = atof(ptr)/1000;

	snprintf(temp, size, "%.02f", t);
	
	close(fd);

	return 0;
}
