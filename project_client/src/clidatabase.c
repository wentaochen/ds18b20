#include "clidatabase.h"

sqlite3		*db = NULL;

int create_db(char *db_name)
{	
	int 		rc;
	char		*sql;
	char		*zerrmsg;

	rc = sqlite3_open("client_temperature.db", &db);
	if (rc != SQLITE_OK)
	{
		dbg_printf("Can't open database: %s", sqlite3_errmsg(db));
		return -1;
	}
	else
	{
		printf("Open database successfully\n");
	}

	sql = "create table if not exists temperdata("\
		   "SN				char			not null,"\
		   "TEMPERATURE		text			not null,"\
		   "TIME			real			not null);";

	rc = sqlite3_exec(db,sql,NULL,0,&zerrmsg);
	if(rc != SQLITE_OK)
	{
		dbg_printf("Create table error !!!:%s\n",zerrmsg);
		sqlite3_free(zerrmsg);
	}

	return 1;

}


int insert_data(data_t *data)
{
	char		*zerrmsg = NULL;
	char		sql[128];
	int			rc;

	sprintf(sql, "insert into temperdata(SN, TEMPERATURE, TIME) values('%s', '%s', '%s');", data->devsn, data->temp, data->s_time);
	rc = sqlite3_exec(db, sql, 0, 0, &zerrmsg);
	if (rc != SQLITE_OK)
	{
		dbg_printf("Data_Insert error: %s\n", zerrmsg);
		return -1;
	}
	else
	{
		dbg_printf("Data_Insert successfully\n");
		return 1;
	}
}

void delete_dbdata(void)
{
    char        *zerrmsg = NULL;
    int         rc;
    char        *sql;
    char        **dbResult;
    int         nRow;
    int         nColumn;
    
    sql = "delete from temperdata where rowid in (select rowid from temperdata limit 1);";

    rc = sqlite3_get_table(db,sql,&dbResult,&nRow,&nColumn,&zerrmsg);

    if(rc != SQLITE_OK)
    {
        printf("Delete data from database error !!!:%s\n",zerrmsg);
        sqlite3_free(zerrmsg);
    }
    else
    {
        printf("Delete data from database successfully\n");
    }
}

int pop_db_record(data_t *data)
{
	int			rc;
	char		*zerrmsg = 0;
	char		**dbResult;
	int			nRow;
	int			nColumn;
	int			i;
	int			j;
	int			index = 0;
	char		sql[64];
	
	sprintf(sql, "select * from temperdata limit 1;");
	rc = sqlite3_get_table(db, sql, &dbResult, &nRow, &nColumn, &zerrmsg);
	if (rc != SQLITE_OK)
	{
		dbg_printf("get tabledata from temperdata failure: %s\n", zerrmsg);
		sqlite3_free(zerrmsg);
		return -1;
	}
	else
	{
		index = nColumn + 1;
		strncpy(data->temp, dbResult[index], 32);
		dbg_printf("data->temp %s\n", data->temp);
		index++;
		strncpy(data->s_time, dbResult[index], 32);
		dbg_printf("data->s_time %s\n", data->s_time);
	
		return 1;
	}

}


int query_dbdata()
{
    char        *zErrMsg = 0;
    int         ret;
    char        *sql;
    char        **dbResult;
    int         nRow;
    int         nColumn;

    sql = "select * from temperdata;";
    ret = sqlite3_get_table(db,sql,&dbResult,&nRow,&nColumn,&zErrMsg);
    if(ret != SQLITE_OK)
    {
        dbg_printf("Judge data exists error !!!:%s\n",zErrMsg);
        sqlite3_free(zErrMsg);
        return -2;
    }
    else
    {
        if(nRow > 0)
        {
			dbg_printf("nRow is %d\n", nRow);
			dbg_printf("database is not null\n");

			return 1;
        }
        else
        {	
			return -1;
        }
    	
    }
}


