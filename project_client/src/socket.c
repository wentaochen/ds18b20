#include "socket.h"

int socket_init(socket_t *sock, char *hostname, int port)
{
	int			rv;

    sock->socketfd = -1;
	sock->servip = hostname;
	sock->port = port;

	return 0;
}


int hostname_to_ip(char **hostname)
{	
	struct hostent		*getname;

	getname = gethostbyname(*hostname);
	if(!getname) 
	{   
		printf("gethostbyname Domain_Name[%s] failure: %s\n", *hostname, strerror(errno)); 
	}

	if ( !(*hostname= inet_ntoa(*(struct in_addr*)getname->h_addr)))
	{   
		printf("inet_ntoa error!\n");
	}
	printf("get servip : %s successfully\n", *hostname);
}


int judge_ip_hostname(char *servip)
{
	unsigned long		addr;

	addr = inet_addr(servip);
	if (addr == INADDR_NONE)
	{
		printf("It is hostname: %s\n", servip);
		return -1;
	}
	else
	{
		printf("It is IP address: %s\n", servip);
		return 1;
	}
}


int socket_connect(socket_t *sock)
{
    int                 rv = -1;
    struct sockaddr_in  servaddr;

    sock->socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sock->socketfd < 0)
    {
        printf("create socket failure: %s\n", strerror(errno));
		socket_close(sock);
		return -1;
    }

    printf("create socket[%d] successfully\n", sock->socketfd);
    
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(sock->port);
    inet_aton(sock->servip, &(servaddr.sin_addr));
	
	/* 判断是否是域名，并进行域名解析 */
	if ((rv = judge_ip_hostname(sock->servip)) < 0)
	{  
		hostname_to_ip(&sock->servip);
	}
	else
	{	
		printf("sock.servip %s\n", sock->servip);
	}

    
    rv = connect(sock->socketfd, (struct sockaddr *)&servaddr, sizeof(servaddr));
    if(rv < 0)
    {
        printf("connect to server[%s: %d] failure: %s\n", sock->servip, sock->port, strerror(errno));
		socket_close(sock);
    }
    else
	{
		printf("connect to server[%s: %d] successfully\n", sock->servip, sock->port);  
	}

	return rv;
}


int judge_connect(int socketfd)
{
	struct tcp_info info;//其实我们就是使用到tcp_info结构体的tcpi_state成员变量来存取socket的连接状态，
	int len = sizeof(info);//如果此返回值为1，则说明socket连接正常，如果返回值为0，则说明连接异常。

	if (socketfd <= 0)
	{
		return -1;
	}

	getsockopt(socketfd, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *) & len);
	if((info.tcpi_state == 1))	//此处也可用info.tcpi_state==TCP_ESTABLISHED,但是头文件需要修改
	{
		return 1;
	} 
	else 		//if(info.tcpi_state != TCP_ESTABLISHED)
	{
		dbg_printf("judge socket disconnected\n");
		return -1;
	}
}


#if 1
int socket_sent_data(socket_t *sock, data_t *data)
{
	char		msg[LEN];
	int			rv;
	
	memset(msg, 0, sizeof(msg));
	snprintf(msg, sizeof(msg), "#%s@%s°C@%s*", data->devsn, data->temp, data->s_time);

	if ((rv = write(sock->socketfd, msg, strlen(msg))) < 0)	
	{
		socket_close(sock);
	}
	
	dbg_printf("msg: %s\n", msg);
	return rv;
}
#endif


int socket_close(socket_t *sock)
{
    close(sock->socketfd);
    sock->socketfd = -1;
}
