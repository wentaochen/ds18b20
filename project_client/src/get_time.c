#include "get_time.h"

int get_time(data_t *data)
{
	time_t		now = time(NULL);
	struct tm	*p = localtime(&now);

    snprintf(data->s_time, 32, "%04d-%02d-%02d %02d:%02d:%02d", p->tm_year + 1900, p->tm_mon + 1, p->tm_mday, p->tm_hour, p->tm_min, p->tm_sec);
	printf("%s\n", data->s_time);

}

