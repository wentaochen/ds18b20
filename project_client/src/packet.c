/*********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  packet.c
 *    Description:  This file is packing data.
 *                 
 *        Version:  1.0.0(19/04/23)
 *         Author:  ChenWentao <11>
 *      ChangeLog:  1, Release initial version on "19/04/23 15:09:09"
 *                 
 ********************************************************************************/

#include "packet.h"

int get_sample_data(data_t *data)
{
	int			rv;

	if ((rv = get_temp(data->temp, sizeof(data->temp))) < 0)
	{
		dbg_printf("get temp failure: %s\n", strerror(errno));
		return -1;
	}
	else
	{
		dbg_printf("temp: %s\n", data->temp);
	}

	if ((rv = get_time(data)) < 0)
	{
		dbg_printf("get time failure: %s\n", strerror(errno));
		return -1;
	}
	else
	{
		dbg_printf("time: %s\n", data->s_time);
	}
}


