#include "main.h"

int g_alarm_flag = 1;

int main(int argc, char **argv)
{
	int					rv = 0;
	int					port = 7788;
	int					opts;
	int					interval = 10;
	int					sample_flag = 0;
	char				*hostname = "studio.iot-yun.club";
	char				*sn = "PRI0001";
	struct data_s		data;
	struct socket_s		sock;

	struct option		long_options[] = 
	{	
		{"devsn", no_argument, NULL, 's'},
		{"hostname", no_argument, NULL, 'H'},
		{"interval", no_argument, NULL, 't'},
		{"port", no_argument, NULL, 'p'},
		{"help", no_argument, NULL, 'h'},
		{NULL, 0, NULL, 0}
	};


	while((opts = getopt_long(argc, argv, "s:H:t:p:h", long_options, NULL)) != -1)
	{
		switch(opts)
		{
			case 's':
				sn = optarg;
				break;
			case 'H':
				hostname = optarg;
				break;
			case 't':
				interval = atoi(optarg);
				break;
			case 'p':
				port = atoi(optarg);
				break;
			case 'h':
				print_usage(argv[0]);
				break;
			default:
				break;
		}
	}

	signal(SIGINT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGPIPE, SIG_IGN);
	signal(SIGALRM, sig_hander);
	
#if 0
	if (!ip || !port)
	{
		print_usage(argv[0]);
		return 0;
	}
#endif

	if ((socket_init(&sock, hostname, port)) < 0)
	{
		return 0;
	}
	strncpy(data.devsn, sn, sizeof(data.devsn));
	create_db(data.devsn);
	socket_connect(&sock);

	while (1)
	{
		sample_flag = 0;

		if(g_alarm_flag == 1)	
		{	
			get_sample_data(&data);
			
			alarm(interval);
			sample_flag = 1;
			g_alarm_flag = 0;
		}

		if ((rv = judge_connect(sock.socketfd)) < 0)		
		{
			if (sock.socketfd > 0)
			{
				socket_close(&sock);
			}

			if ((rv = socket_connect(&sock)) == 0)
			{
				dbg_printf("scoket client connect again successfully\n");
			}
			else
			{	
				socket_close(&sock);
				dbg_printf("socket client connect again failure\n");
			}
		}

		if (sock.socketfd < 0)
		{
			if (sample_flag)
			{
				if ((rv = insert_data(&data)) < 0)
				{
					dbg_printf("Inser_Data failure: %s\n", strerror(errno));
				}
			}

			continue;
		}

		if (sample_flag)
		{
			if ((rv = socket_sent_data(&sock, &data)) < 0)
			{
				if ((rv = insert_data(&data)) < 0)
				{
					dbg_printf("Inser_Data failure: %s\n", strerror(errno));
				}
				continue;
			}
		}

		if ((rv = query_dbdata()) > 0)	//判断数据库是否有数据
		{	
			pop_db_record(&data);	//弹出第一条数据
			if ((rv = socket_sent_data(&sock, &data)) < 0)
			{
				socket_close(&sock);
				continue;
			}
			delete_dbdata();
		}
	}//while(1)
	return 0;
}//main


void print_usage(char *progname)
{
	printf("%s usage:\n", progname);
	printf("-H(--ipaddr): sepcify IP address or hostname. \n");
	printf("-p(--port): sepcify port. \n");
	printf("-t(--time):sepcify time is help information. \n");
	printf("-s(--devsn): sepcify devsn. \n");
}


void sig_hander(int signum)
{
	dbg_printf("\n");	
	dbg_printf("Time is up, is time to sample\n");
	g_alarm_flag = 1;
}
