/********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  packet.h
 *    Description:  This file is packet.c header file.
 *
 *        Version:  1.0.0(19/04/23)
 *         Author:  ChenWentao <11>
 *      ChangeLog:  1, Release initial version on "19/04/23 15:20:29"
 *                 
 ********************************************************************************/

#ifndef __PACKET_H__
#define __PACKET_H__

#include "clidatabase.h"
#include "get_time.h"
#include "get_temp.h"

#define CONFIG_DEBUG
#ifdef  CONFIG_DEBUG
#define dbg_printf(format, ...) printf(format, ##__VA_ARGS__)
#else
#define dbg_printf(format, ...) do{} while{0}
#endif

int get_sample_data(data_t *data);

#endif
