#ifndef __CLIDATABASE_H__
#define __CLIDATABASE_H__

#include <stdio.h>
#include <sqlite3.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


#define CONFIG_DEBUG
#ifdef  CONFIG_DEBUG
#define dbg_printf(format, ...)	printf(format, ##__VA_ARGS__)
#else
#define dbg_printf(format, ...)	do{} while{0}
#endif

#if 1 
typedef struct data_s
{
        char    devsn[16];
        char	temp[16];
        char    s_time[32];
}data_t;
#endif

int create_db(char *db_name);
int insert_data(data_t *data);
void delete_dbdata();
int pop_db_record(data_t *data);
int query_dbdata(void);
#endif
