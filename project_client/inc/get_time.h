#ifndef __GET_TIME_H__
#define __GET_TIME_H__

#include <time.h>
#include "clidatabase.h"

extern int get_time(data_t *data);

#endif
