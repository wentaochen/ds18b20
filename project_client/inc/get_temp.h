#ifndef __GET_TEMP_H__
#define __GET_TEMP_H__

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

extern int get_temp(char *t, int size);


#endif
