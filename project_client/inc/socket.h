#ifndef __SOCKET_H__
#define __SOCKET_H__

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <getopt.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <linux/tcp.h>
#include <libgen.h>
#include "clidatabase.h"

#define LEN	256

typedef struct socket_s
{
	int		socketfd;	 /* socket connect status, 0: disconnected,1:connected*/
	char	*servip;	/* socket server hostname or IP address*/
	int		port;		/* socket server listen port */
}socket_t;

int socket_init(socket_t *sock, char *hostname, int port);
int socket_connect(socket_t *sock);
int judge_connect(int socketfd);
int socket_close(socket_t *sock);
int judge_ip_hostname(char *servip);
int hostname_to_ip(char **hostname);
int socket_sent_data(socket_t *sock, data_t *data);
#endif	


	
	
