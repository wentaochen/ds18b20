#ifndef	__MAIN_H__
#define __MAIN_H__

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include "socket.h"
#include "clidatabase.h"
#include  "packet.h"

#define LEN	256

#define CONFIG_DEBUG
#ifdef	CONFIG_DEBUG
#define dbg_printf(format, ...) printf(format, ##__VA_ARGS__)
#else
#define dbg_printf(format, ...) do{} while{0}
#endif


extern void print_usage(char *progname);
extern void stop(int signum);
void sig_hander(int signum);
#endif
