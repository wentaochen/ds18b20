#ifndef __CUT_DATA_H__
#define __CUT_DATA_H__

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "serdatabase.h"

#define CONFIG_DEBUG
#ifdef  CONFIG_DEBUG
#define dbg_printf(format,...) printf(format, ##__VA_ARGS__)
#else      /* -----  not CONFIG_DEBUG  ----- */
#define dbg_printf(format,...) do{} while(0)
#endif     /* -----  not CONFIG_DEBUG  ----- */

int cut_data(char *buf, data_t *data, data_t *stic_data);

#endif
