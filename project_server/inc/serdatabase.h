#ifndef	__SERDATABASE_H__
#define __SERDATABASE_H__

#include <stdio.h>
#include <sqlite3.h>
#include <stdlib.h>

#define CONFIG_DEBUG
#ifdef  CONFIG_DEBUG
#define dbg_printf(format,...) printf(format, ##__VA_ARGS__)
#else      /* -----  not CONFIG_DEBUG  ----- */
#define dbg_printf(format,...) do{} while(0)
#endif     /* -----  not CONFIG_DEBUG  ----- */

typedef struct data_s
{
	char	sn[16];
	char	temp[16];
	char	time[32];
}data_t;

int create_db(data_t *data);
int insert_serdata(data_t *data);

#endif
