#ifndef	__SOCKET_SERVER_H__
#define __SOCKET_SERVER_H__

#include <stdio.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <libgen.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/resource.h>

#define MAX_EVENTS      1024
#define ARRAY_SIZE(x)   sizeof(x)/sizeof(x[0])

#define CONFIG_DEBUG
#ifdef  CONFIG_DEBUG
#define dbg_printf(format,...) printf(format, ##__VA_ARGS__)
#else      /* -----  not CONFIG_DEBUG  ----- */
#define dbg_printf(format,...) do{} while(0)
#endif     /* -----  not CONFIG_DEBUG  ----- */

typedef struct socket_s
{
	int	port;
	int	socketfd;
	int     clifd;
}socket_t;

typedef struct epoll_s
{
	int     		epollfd;
        int     		events;
	struct epoll_event      event;
        struct epoll_event      event_array[MAX_EVENTS];
}epoll_t;

void socket_init(socket_t *sock, int port);
int socket_listen(socket_t *sock);
void socket_close(int *socketfd);
int epoll_init(socket_t *sock, epoll_t *mul);
int judge_events(epoll_t *mul);
void set_socket_rlimit(void);

#endif
