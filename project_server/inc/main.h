#ifndef __MAIN_H__
#define __MAIN_H

#include <stdio.h>
#include "socket_serconnect.h"
#include "serdatabase.h"
#include <signal.h>
#include "cut_data.h"

void stop(int);
void print_usage(char *progname);

#endif
