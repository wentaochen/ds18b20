#include "main.h"

int	pro_stop = 0;

int main(int argc, char **argv)
{
        int			rv;
        int                     port = 0;
        int                     opt;
        int                     daemon_run = 0;
	char			*progname = NULL;
        int                     epollfd;
        int                     events;
        int                     i, j;
        int                     clifd = -1;
        char                    buf[1024];
	char			sticky_data[128] = {0};
	data_t			data;
	data_t			stic_data;
	struct socket_s		sock;
	struct epoll_s		mul;
        struct epoll_event      event;
        struct epoll_event      event_array[MAX_EVENTS];
        socklen_t               len;
        struct sockaddr_in      cliaddr;
        struct option           long_options[] =
	{
        	{"port", required_argument, NULL, 'p'},
                {"daemon", no_argument, NULL, 'b'},
                {"help", no_argument, NULL, 'h'},
                {NULL, 0, NULL, 0}
        };
	
	progname = basename(argv[0]);

        while((opt = getopt_long(argc, argv, "p:h", long_options, NULL)) != -1)
        {
                switch(opt)
                {
                        case 'p':
                                port = atoi(optarg);
                                break;
                        case 'b':
                                daemon_run = 1;
                                break;
                        case 'h':
                                print_usage(argv[0]);
                                return EXIT_SUCCESS;
                        default:
                                break;
                }
        }

        if(!port)
        {
                print_usage(argv[0]);
                return 0;
        }
	
	if (daemon_run)
	{
		daemon(0, 0);
	}
	
	signal(SIGINT, stop);
	signal(SIGTERM, stop);

	set_socket_rlimit();

	socket_init(&sock, port);
	if ((rv = socket_listen(&sock)) < 0)
	{
		return 0;
	}

	if ((rv = epoll_init(&sock, &mul)) < 0)
	{
		return -1;
	}

        while(!pro_stop)
        {	
		if (( rv = judge_events(&mul)) > 0)
		{
			for(i = 0; i < mul.events; i++)
			{
				if((mul.event_array[i].events & EPOLLERR) || (mul.event_array[i].events & EPOLLHUP))
				{
					dbg_printf("epoll_wait get error on fd[%d]: %s\n", mul.event_array[i].data.fd, strerror(errno));
					epoll_ctl(mul.epollfd, EPOLL_CTL_DEL, mul.event_array[i].data.fd, NULL);
					close(mul.event_array[i].data.fd);
				}
				if(mul.event_array[i].data.fd == sock.socketfd)
				{
					sock.clifd = accept(sock.socketfd, (struct sockaddr *)NULL, NULL);
					if(sock.clifd < 0)
					{
						dbg_printf("accept new client failure: %s\n", strerror(errno));
						continue;
					}

					mul.event.data.fd = sock.clifd;
					mul.event.events = EPOLLIN;

					if(epoll_ctl(mul.epollfd, EPOLL_CTL_ADD, sock.clifd, &mul.event) < 0)
					{
						dbg_printf("epoll add client socket failure: %s\n", strerror(errno));
						close(mul.event_array[i].data.fd);
						continue;
					}
					printf("epoll add new client socket[%d] ok.\n", sock.clifd);
				}
				else
				{
					memset(buf, 0, sizeof(buf));
					if((rv = read(mul.event_array[i].data.fd, buf, sizeof(buf))) <= 0)
					{
						dbg_printf("socket[%d] read failure or get disconnect and will be remove.\n", mul.event_array[i].data.fd);
						epoll_ctl(mul.epollfd, EPOLL_CTL_DEL, mul.event_array[i].data.fd, NULL);
						close(mul.event_array[i].data.fd);
						continue;
					}
					else
					{
						create_db(&data);
						dbg_printf("socket[%d] read get %d bytes data\n", mul.event_array[i].data.fd, rv);
						dbg_printf("buf: %s\n", buf);
						
						memset(&stic_data, 0, sizeof(stic_data));
						if((rv = cut_data(buf, &data, &stic_data)) > 0)
						{
							dbg_printf("cut_data successfully\n");
							dbg_printf("data->SN main %s\n", data.sn);
							dbg_printf("data->TEMP main %s\n", data.temp);
							dbg_printf("data->TIME main %s\n", data.time);

							if((rv = insert_serdata(&data)) > 0)
							{
								dbg_printf("Insert_Serdata successfully\n");
							}
							
							if (strlen(stic_data.sn) != 0)
							{
								if((rv = insert_serdata(&stic_data)) > 0)
								{
									dbg_printf("Insert_Serdata successfully\n");
								}

							}
						}
					}
					//if((cut_data(buf) < 0)
				}
			}  //for (i=0; i<events; i++)
		}//if (( rv = judge_events(&mul)) > 0)

		continue;

	}  //while(1)

	return 0;
}

void stop(int signum)
{
	pro_stop = 1;
}

void print_usage(char *progname)
{
	printf("%s usage:\n", progname);
	printf("-p(--port): sepcify server.\n");
	printf("-d(--daemon): set proname running on background.\n");
	printf("-h(--help): print this help information.\n");
}
