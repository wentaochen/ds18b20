#include "cut_data.h"

int cut_data(char *buf, data_t *data, data_t *stic_data)
{
        char            sept[] = "@";
	char		head = '#';
	char		tail = '*';
	char		*value;
	int		i = 0;
	int		j = 0;
	int 		temp_head = 0;
	int		temp_tail = 0;
	char		data_packet[128] = {0};
	char		sticky_data[128] = {0};

	for(i=0; i<strlen(buf); i++)
	{
		if(buf[i] == head)
		{
			temp_head = i;
			printf("data head pod : %d\n",temp_head);
			for(j=i+1; j<strlen(buf); j++)
			{
				if(buf[j] == tail)
				{
					temp_tail = j;
					printf("data tail pod : %d\n",temp_tail);
					break;
				}
			}

			break;
		}
	}

	if(temp_tail > temp_head && temp_tail <= strlen(buf))
	{
		strncpy(data_packet, &buf[temp_head+1], temp_tail - temp_head - 1);
		printf("data packet :%s\n",data_packet);
		if (buf[temp_tail + 1] == '#')
		{
			strncpy(sticky_data, &buf[temp_tail+2], strlen(buf) - temp_tail - 2);
			printf("sticky data :%s\n",sticky_data);

			value = strtok(sticky_data, sept);
			strncpy(stic_data->sn, value, sizeof(stic_data->sn));
			dbg_printf("SN: %s\n", stic_data->sn);

			value = strtok(NULL, sept);
			strncpy(stic_data->temp, value, sizeof(stic_data->temp));
			dbg_printf("TEMP: %s\n", stic_data->temp);

			value = strtok(NULL, "\n");
			strncpy(stic_data->time, value, sizeof(stic_data->time));
			dbg_printf("TIME: %s\n", stic_data->time);
		}
	}

	value = strtok(data_packet, sept);
	strncpy(data->sn, value, sizeof(data->sn));
	dbg_printf("SN: %s\n", data->sn);

	value = strtok(NULL, sept);
	strncpy(data->temp, value, sizeof(data->temp));
	dbg_printf("TEMP: %s\n", data->temp);

	value = strtok(NULL, "\n");
	strncpy(data->time, value, sizeof(data->time));
	dbg_printf("TIME: %s\n", data->time);

	return 1;
}
