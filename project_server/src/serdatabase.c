#include "serdatabase.h"

sqlite3		*db = NULL;

int create_db(data_t *data)
{
	int		rc;
	char		sql[128];
	char		*zerrmsg = NULL;

	rc = sqlite3_open("serdatabase.db", &db);
	if(rc)
	{
		dbg_printf("Open database failure: %s", sqlite3_errmsg(db));
	}
	else
	{
		dbg_printf("Open database successfully\n");
	}

	sprintf(sql, "create table if not exists %s(SN char, TEMPERATURE text, TIME real);", data->sn);

        rc = sqlite3_exec(db, sql, 0, 0, &zerrmsg);
        if(rc != SQLITE_OK)
        {
                dbg_printf("Table create failure: %s\n", zerrmsg);
                return -1;
        }
        else
        {
                dbg_printf("Table create successfully\n");
		return 1;
	}
}


int insert_serdata(data_t *data)
{
	int		rc;
	char		sql[256];
	char		*zerrmsg = NULL;
	
	snprintf(sql, sizeof(sql), "insert into %s(SN, TEMPERATURE, TIME) values('%s', '%s', '%s');", data->sn, data->sn, data->temp, data->time);
	
	dbg_printf("SN:%s TEMP:%s TIME:%s\n", data->sn, data->temp, data->time);
        rc = sqlite3_exec(db, sql, 0, 0, &zerrmsg);
        if (rc != SQLITE_OK)
        {
                dbg_printf("Data_Insert error: %s\n", zerrmsg);
                return -1;
        }
        else
        {
                dbg_printf("Data_Insert successfully\n");
        }	
	
	return 1;
}

