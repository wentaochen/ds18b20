#include "socket_serconnect.h"

void socket_init(socket_t *sock, int port)
{
	sock->port = port;
	sock->socketfd = -1;
}

int socket_listen(socket_t *sock)
{
        int                     on = 65536;
        struct sockaddr_in      servaddr;
	int			rv;

        sock->socketfd = socket(AF_INET, SOCK_STREAM, 0);
        if(sock->socketfd < 0)
        {
                dbg_printf("create soceket failure: %s\n", strerror(errno));
		socket_close(&sock->socketfd);
                return -1;
        }
        dbg_printf("create socket[%d] successfully\n", sock->socketfd);

        setsockopt(sock->socketfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

        memset(&servaddr, 0, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(sock->port);
        servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

        rv = bind(sock->socketfd, (struct sockaddr *)&servaddr, sizeof(servaddr));
        if(rv < 0)
        {
		dbg_printf("socket[%d] bind on port[%d] failure: %s\n", sock->socketfd, sock->port, strerror(errno));
		socket_close(&sock->socketfd);
		return -2;
	}

	listen(sock->socketfd, 64);
	dbg_printf("Start to listen on port [%d]!\n", sock->port);

	return sock->socketfd;

}


void socket_close(int *socketfd)
{
	close(*socketfd);
	*socketfd = -1;
}

int epoll_init(socket_t *sock, epoll_t *mul)
{
	if ((mul->epollfd = epoll_create(MAX_EVENTS)) < 0)
	{
		dbg_printf("epoll create failure: %s\n", strerror(errno));
		return -1;
	}

	mul->event.events = EPOLLIN;
	mul->event.data.fd = sock->socketfd;

	if (epoll_ctl(mul->epollfd, EPOLL_CTL_ADD, sock->socketfd, &mul->event) < 0)
	{
		dbg_printf("epoll add listen socket failure: %s\n", strerror(errno));
		return -1;
	}
	return 1;
}

int judge_events(epoll_t *mul)
{
	mul->events = epoll_wait(mul->epollfd, mul->event_array, MAX_EVENTS, -1);
	if(mul->events < 0)
	{
		dbg_printf("epoll failure: %s\n", strerror(errno));
	}
	else if(mul->events == 0)
	{
		dbg_printf("epoll get timeout!!!\n");
	}

	return mul->events;
}

void set_socket_rlimit(void)
{
        struct rlimit limit = {0};

        getrlimit(RLIMIT_NOFILE, &limit);
        limit.rlim_cur = limit.rlim_max;
        setrlimit(RLIMIT_NOFILE, &limit);

        printf("set socket open fd max count to %ld\n", limit.rlim_max);
}
